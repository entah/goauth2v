TOOLS_DEPS = github.com/securego/gosec/cmd/gosec golang.org/x/lint/golint golang.org/x/tools/cmd/cover honnef.co/go/tools/cmd/staticcheck


.PHONY: all deps test coverage clean security staticcheck lint

all: deps clean test security staticcheck lint coverage

deps: ## Fetch modules needed.
	@go get -v -d ./... &&\
	echo "Installing dev dependencies." &&\
	GO111MODULE=off go get -v -d $(TOOLS_DEPS)

test: ## Run unit testing.
	@go test -race ./...

coverage: ## Run unit testing with coverage profile.
	@mkdir -p cover &&\
	go test ./... -covermode=count -coverprofile cover/coverage.cov 1> /dev/null &&\
	go tool cover -func=cover/coverage.cov

clean: ## Clean generated-files and test caches.
	@rm -rf
	@go clean --testcache &&\
	go mod tidy

security: ## Run security check.
	@gosec --no-fail -quiet ./...

staticcheck: ## Run static check.
	@staticcheck ./...

lint: ## Run linter.
	@golint ./...

bench: ## Run Benchmark Test.
	@go test -bench=. -benchmem ./...
