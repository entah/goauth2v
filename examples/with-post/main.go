package main

import (
	"bytes"
	"context"
	"html/template"
	"log"
	"os"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/entah/goauth2v"
)

var indexTemplate = `
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <link rel="icon" type="image/x-icon" href="/assets/favicon.ico" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>entah@signwithgoogle</title>
</head>

<div id="g_id_onload"
     data-client_id="{{ . }}"
     data-context="signin"
     data-ux_mode="popup"
     data-callback="handleCredentialResponse"
     data-itp_support="true">
</div>

<div class="g_id_signin"
     data-type="standard"
     data-shape="rectangular"
     data-theme="outline"
     data-text="signin_with"
     data-size="large"
     data-logo_alignment="left">
</div>
</br>
</br>
<div id="claims">
</div>

<script src="https://accounts.google.com/gsi/client" async defer></script>
<script>
  async function handleCredentialResponse(response) {
  try {
    const resp = await fetch("/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(response)
    });

    const result = await resp.text();
    const el = document.getElementById("claims");
    el.innerText = result;
    console.log("Success:", result);
  } catch (error) {
    alert(error);
    console.error("Error:", error);
  }
}
</script>
</body>

</html>
`

func HandleGOAuth2(validator *goauth2v.GoogleOAuth2Validator) echo.HandlerFunc {
	return func(c echo.Context) error {
		reqLoad := struct {
			Credential string `json:"credential"`
			ClientId   string `json:"client_id"`
		}{}

		if err := c.Bind(&reqLoad); err != nil {
			log.Println(err)
			return err
		}

		log.Printf("%#v", reqLoad)

		claim, err := validator.Validate(c.Request().Context(), reqLoad.Credential)
		if err != nil {
			log.Println(err)
			return err
		}

		log.Printf("%#v", claim)

		return c.JSONPretty(200, claim, " ")
	}
}

func main() {

	if len(os.Args) < 2 {
		panic("second argument (client id) should be provided")
	}

	clientId := os.Args[1]

	goauth2 := goauth2v.NewGoogleOAuth2Validator(context.Background(), clientId)
	templ := template.New("index.html")
	index, err := templ.Parse(indexTemplate)
	if err != nil {
		panic(err)
	}

	var reader bytes.Buffer
	if err := index.Execute(&reader, clientId); err != nil {
		panic(err)
	}

	server := echo.New()
	server.Use(middleware.CORSWithConfig(middleware.DefaultCORSConfig))
	server.Use(middleware.GzipWithConfig(middleware.GzipConfig{Level: 1, Skipper: middleware.DefaultSkipper}))
	server.GET("/", func(c echo.Context) error {
		return c.HTML(200, string(reader.Bytes()))
	})
	server.POST("/login", HandleGOAuth2(goauth2))
	server.Start(":8080")
}
