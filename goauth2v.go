// Package goauth2v implement server-side validation explain in here `https://developers.google.com/identity/gsi/web/guides/verify-google-id-token`.
package goauth2v

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"strings"
	"time"

	"github.com/lestrrat-go/jwx/v2/jwk"
	"github.com/lestrrat-go/jwx/v2/jwt"
)

// GoogleInfoClaim private claim from Google OAUth2 JWT Token.
type GoogleInfoClaim struct {
	AZP           string `json:"AZP"`
	Email         string `json:"email"`
	Name          string `json:"name"`
	EmailVerified bool   `json:"email_verified"`
	FamilyName    string `json:"family_name"`
	GivenName     string `json:"given_name"`
	Picture       string `json:"picture"`
	HD            string `json:"hd"`
}

const googleCerts = "https://www.googleapis.com/oauth2/v3/certs"

// GoogleOAuth2Validator validator for Google OAUth2 JWT Token.
type GoogleOAuth2Validator struct {
	jwkCache *jwk.Cache
	clientId string
}

// Validate parse `token` and return `*GoogleInfoClaim`.
func (goa2v *GoogleOAuth2Validator) Validate(ctx context.Context, token string) (*GoogleInfoClaim, error) {
	set, err := goa2v.jwkCache.Get(ctx, googleCerts)
	if err != nil {
		return nil, err
	}

	parsed, err := jwt.ParseString(
		token,
		jwt.WithKeySet(set),
		jwt.WithAudience(goa2v.clientId),
		jwt.WithAcceptableSkew(time.Second*60),
	)

	if err != nil {
		return nil, err
	}

	if parsed.Issuer() != "accounts.google.com" && parsed.Issuer() != "https://accounts.google.com" {
		return nil, errors.New("issuer not match")
	}

	buff, err := base64.RawStdEncoding.DecodeString(strings.Split(token, ".")[1])
	if err != nil {
		return nil, err
	}

	claim := new(GoogleInfoClaim)
	return claim, json.Unmarshal(buff, &claim)
}

// NewGoogleOAuth2Validator create new `GoogleOAuth2Validator` with provided `clientId`.
func NewGoogleOAuth2Validator(ctx context.Context, clientId string) *GoogleOAuth2Validator {
	if clientId == "" {
		panic("client id should not be empty string")
	}

	jwkCache := jwk.NewCache(ctx)
	if err := jwkCache.Register(googleCerts, jwk.WithMinRefreshInterval(time.Minute*30), jwk.WithRefreshInterval(time.Minute*60)); err != nil {
		panic(err)
	}

	return &GoogleOAuth2Validator{clientId: clientId, jwkCache: jwkCache}
}
